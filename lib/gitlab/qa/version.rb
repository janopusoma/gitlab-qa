# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '9.1.0'
  end
end
