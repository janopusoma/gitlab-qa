# frozen_string_literal: true

describe Gitlab::QA::Report::RelateFailureIssue do
  describe '#invoke!' do
    let(:project) { 'valid-project' }
    let(:test_name) { 'Manage Users API GET /users' }
    let(:test_file_partial) { 'api/1_manage/users_spec.rb' }
    let(:test_file_full) { "./qa/specs/features/#{test_file_partial}" }
    let(:testcase_url) { 'https://gitlab.com/gitlab-org/gitlab/-/quality/test_cases/460' }
    let(:product_group_tag) { nil }
    let(:ci_job_url) { 'https://gitlab.com/gitlab-org/gitlab-qa/-/jobs/806591854' }
    let(:file_upload_class) { Struct.new(:markdown) }
    let(:file_upload) { file_upload_class.new("![failure_screenshot](/uploads/failure_screenshot.png)") }
    let(:failure_class) { 'QA::Support::Repeater::WaitExceededError' }
    let(:failure_message) { 'Page did not fully load. This could be due to an unending async request or loading icon.' }
    let(:failure_lines) do
      [
        'Failure/Error: Page::Project::Wiki::Edit.perform(&:click_submit)',
        '',
        'QA::Support::Repeater::WaitExceededError:',
        '  Page did not fully load. This could be due to an unending async request or loading icon.'
      ]
    end

    let(:test_data) do
      <<~JSON
        {
          "examples": [
            {
              "id":"#{test_file_full}[1:1:1]",
              "description":"GET /users",
              "full_description": "#{test_name}",
              "status":"failed",
              "file_path":"#{test_file_full}",
              "line_number":11,
              "run_time":0.31676485,
              "pending_message":null,
              "testcase":"#{testcase_url}",
              "quarantine":null,
              "screenshot": { "image": "failure_screenshot.png"},
              "product_group":"#{product_group_tag}",
              "ci_job_url":"#{ci_job_url}",
              "exceptions":[
                {
                  "class":"#{failure_class}",
                  "message":"#{failure_message}",
                  "message_lines":#{failure_lines.to_json},
                  "backtrace":[
                    "/usr/local/bundle/gems/airborne-0.3.4/lib/airborne/request_expectations.rb:36:in `expect_status'",
                    "./qa/specs/features/api/1_manage/users_spec.rb:14:in `block (3 levels) in <module:QA>'"
                  ]
                }
              ]
            }
          ]
        }
      JSON
    end

    let(:issue_class) { Struct.new(:iid, :web_url, :state, :title, :description, :labels) }
    let(:new_issue) { issue_class.new(0, 'http://new-issue.url') }
    let(:expected_posted_note) { "Failed most recently in staging pipeline: #{ci_job_url}" }
    let(:expected_new_issue_description) do
      "### Full description\n\n#{test_name}\n\n### File path\n\n#{test_file_full}\n\n" \
      "### Stack trace\n\n```\n#{failure_lines.join("\n")}\n```\n\nFirst happened in #{ci_job_url}.\n\nRelated test case: #{testcase_url}.\n\n### Screenshot: #{file_upload.markdown}"
    end

    shared_examples 'screenshot not included' do
      let(:expected_new_issue_description) do
        "### Full description\n\n#{test_name}\n\n### File path\n\n#{test_file_full}\n\n" \
        "### Stack trace\n\n```\n#{failure_lines.join("\n")}\n```\n\nFirst happened in #{ci_job_url}.\n\nRelated test case: #{testcase_url}.\n\n"
      end

      it 'does not include a screenshot' do
        expect(::Gitlab).not_to receive(:upload_file)
        expect(::Gitlab).to receive(:create_issue)
          .with(
            project,
            "Failure in #{test_file_partial} | #{test_name}",
            hash_including(description: expected_new_issue_description, labels: described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com'], issue_type: 'issue'))
          .and_return(new_issue)

        expect(subject).to receive(:update_labels)
        expect(subject).to receive(:post_or_update_failed_job_note)

        expect { subject.invoke! }.to output.to_stdout
      end
    end

    shared_examples 'issue created with message' do
      let(:expected_new_issue_description_with_message) do
        "### Full description\n\n#{test_name}\n\n### File path\n\n#{test_file_full}\n\n" \
        "### Stack trace\n\n```\n#{failure_class}: #{failure_message}\n```\n\n" \
        "First happened in #{ci_job_url}.\n\nRelated test case: #{testcase_url}.\n\n### Screenshot: #{file_upload.markdown}"
      end

      it 'uses the message to create an issue' do
        expect(::Gitlab).to receive(:upload_file).and_return(file_upload)
        expect(::Gitlab).to receive(:create_issue)
          .with(
            project,
            "Failure in #{test_file_partial} | #{test_name}",
            hash_including(
              description: expected_new_issue_description_with_message,
              issue_type: 'issue',
              labels: described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com']
            ))
          .and_return(new_issue)

        expect(subject).to receive(:update_labels)
        expect(subject).to receive(:post_or_update_failed_job_note)

        expect { subject.invoke! }
          .to output(%r{Created new issue: http://new-issue.url\nfor test '#{test_name}'.*}).to_stdout
      end
    end

    shared_examples 'new issue created' do
      it 'creates issue' do
        expect(::Gitlab).to receive(:upload_file).and_return(file_upload)
        expect(::Gitlab).to receive(:create_issue).and_return(new_issue)
        expect(subject).to receive(:update_labels)
        expect(subject).to receive(:post_or_update_failed_job_note)

        expect { subject.invoke! }
          .to output(%r{Created new issue: http://new-issue.url\nfor test '#{test_name}'.*}).to_stdout
      end
    end

    shared_examples 'error(s) can be ignored' do
      it 'skips reporting' do
        expect(::Gitlab).not_to receive(:create_issue)

        expect { subject.invoke! }
          .to output(/Failure reporting skipped because the #{failure_reason}/).to_stdout
      end
    end

    around do |example|
      ClimateControl.modify(CI_PROJECT_NAME: 'staging') { example.run }
    end

    describe 'NEW_ISSUE_LABELS' do
      it { expect(described_class::NEW_ISSUE_LABELS).to eq(Set.new(%w[QA Quality test failure::new priority::2])) }
    end

    context 'with valid input' do
      subject { described_class.new(token: 'token', input_files: 'files', project: project) }

      before do
        allow(subject).to receive(:assert_input_files!)
        allow(subject.__send__(:gitlab)).to receive(:assert_user_permission!)
        allow(::Dir).to receive(:glob).and_return([test_file])
        allow(::File).to receive(:read).with(test_file).and_return(test_data)
      end

      context 'when an issue exists for a given test' do
        context 'when the results are in JSON format' do
          let(:test_file) { 'file.json' }
          let(:issues) { [] }
          let(:issue_description) do
            <<~TEXT
              ### Full description

              Manage Users API GET /users

              ### File path

              ./qa/specs/features/api/1_manage/users_spec.rb

              ### Stack trace

              ```
              Failure/Error: Page::Project::Wiki::Edit.perform(&:click_submit)

              QA::Support::Repeater::WaitExceededError:
                Page did not fully load. This could be due to an unending async request or loading icon.
              ```
            TEXT
          end

          before do
            allow(::File).to receive(:write)
            allow(::Gitlab).to receive(:issues)
              .with(anything, { state: 'opened', labels: 'QA' })
              .and_return(Struct.new(:auto_paginate).new(issues))
          end

          context 'when no failures are present' do
            let(:test_data) do
              <<~JSON
                {
                  "examples": [
                    {
                      "full_description": "#{test_name}",
                      "file_path": "#{test_file_full}"
                    }
                  ]
                }
              JSON
            end

            it 'does not find the issue' do
              expect { subject.invoke! }.not_to output(/Found issue/).to_stdout
            end
          end

          context 'when no issue is found' do
            it_behaves_like 'new issue created'

            context 'when creating a new issue' do
              it 'creates the issue in the provided project and posts the failed job url' do
                expect(::Gitlab).to receive(:upload_file).and_return(file_upload)
                expect(::Gitlab).to receive(:create_issue)
                  .with(
                    project,
                    "Failure in #{test_file_partial} | #{test_name}",
                    hash_including(description: expected_new_issue_description, labels: described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com'], issue_type: 'issue'))
                  .and_return(new_issue)

                expect(::Gitlab).to receive(:edit_issue).with(anything, anything, { labels: %w[found:staging.gitlab.com] })
                expect(subject).to receive(:post_or_update_failed_job_note).and_call_original
                expect(subject).to receive(:existing_failure_note).and_return(false)
                expect(::Gitlab).to receive(:create_issue_note).with(project, new_issue.iid, expected_posted_note)

                expect { subject.invoke! }.to output.to_stdout
              end

              it 'includes the failure screenshot in the description' do
                expect(::Gitlab).to receive(:upload_file)
                  .with(project, 'failure_screenshot.png')
                  .and_return(file_upload)

                expect(::Gitlab).to receive(:create_issue)
                  .with(
                    project,
                    "Failure in #{test_file_partial} | #{test_name}",
                    hash_including(description: expected_new_issue_description, labels: described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com'], issue_type: 'issue'))
                  .and_return(new_issue)

                expect(subject).to receive(:update_labels)
                expect(subject).to receive(:post_or_update_failed_job_note)

                expect { subject.invoke! }.to output.to_stdout
              end

              context 'when the error is a 500 Internal Server Error' do
                let(:failure_lines) { ["Failed to GET group - (500): { 'message' : '500 Internal Server Error' }."] }

                it_behaves_like 'screenshot not included'
              end

              context 'when the error is from fabricate_via_api!' do
                let(:failure_lines) do
                  [
                    'Failure/Error:',
                    '  Resource::Project.fabricate_via_api! do |project|',
                    '    project.visibility = :private',
                    '  end'
                  ]
                end

                it_behaves_like 'screenshot not included'
              end

              context 'when the report has an empty stacktrace' do
                let(:failure_lines) { [] }

                it_behaves_like 'issue created with message'
              end

              context 'when the report has only a string in stacktrace' do
                let(:failure_lines) { 'Shared Example Group' }

                it_behaves_like 'issue created with message'
              end

              context 'when the issue title would be longer than the maximum allowed' do
                let(:test_file_partial) { 'api/3_create/gitaly/changing_repository_storage_spec.rb' }
                let(:test_name) do
                  'Create Changing Gitaly repository storage when moving from Gitaly to Gitaly Cluster behaves like ' \
                  'repository storage move confirms a `finished` status after moving project repository storage'
                end

                it 'creates the issue in the provided project and post the failed job url' do
                  full_title = "Failure in #{test_file_partial} | #{test_name}"

                  expect(::Gitlab).to receive(:upload_file).and_return(file_upload)
                  expect(::Gitlab).to receive(:create_issue)
                    .with(project, "#{full_title[0...described_class::MAX_TITLE_LENGTH - 3]}...", hash_including(issue_type: 'issue'))
                    .and_return(new_issue)

                  expect(::Gitlab).to receive(:edit_issue).with(anything, anything, { labels: %w[found:staging.gitlab.com] })
                  expect(subject).to receive(:post_or_update_failed_job_note)

                  expect { subject.invoke! }.to output.to_stdout
                end
              end

              context 'when product group is specified' do
                let(:product_group_tag) { 'source_code' }
                let(:set_username) { 'jane_smith' }
                let(:user_id) { 23 }

                it 'assigns the issue to a SET and provides a due date' do
                  expect(::Gitlab).to receive(:upload_file).and_return(file_upload)
                  expect(::Gitlab).to receive(:create_issue).and_return(new_issue)
                  expect(subject).to receive(:update_labels)
                  expect(subject).to receive(:post_or_update_failed_job_note)

                  expect(subject).to receive(:assign_dri).and_call_original
                  expect(subject).to receive(:set_dri_via_group).and_return(set_username)
                  expect_any_instance_of(::Gitlab::QA::Report::GitlabIssueClient).to receive(:find_user_id).with(username: set_username).and_return(user_id)
                  expect(::Gitlab)
                    .to receive(:edit_issue)
                    .with(project, new_issue.iid, hash_including(assignee_id: user_id, due_date: Date.today + 1.month))

                  expect { subject.invoke! }.to output.to_stdout
                end
              end
            end
          end

          context 'when one issue is found' do
            let(:issue) do
              issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', issue_title, issue_description)
            end

            let(:issues) { [issue] }

            before do
              allow(subject).to receive(:update_labels)
              allow(subject).to receive(:post_or_update_failed_job_note)
            end

            context 'when test name matches issue title' do
              let(:issue_title) { "Hello #{test_name} world!" }

              it 'finds the issue' do
                expect { subject.invoke! }.to output(/Found issue #{issue.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
              end

              it 'posts the failed job url' do
                expect(subject).to receive(:update_labels).and_call_original
                expect(::Gitlab).to receive(:edit_issue).with(anything, anything, { labels: ['found:staging.gitlab.com'] })
                expect(subject).to receive(:post_or_update_failed_job_note).and_call_original
                expect(subject).to receive(:existing_failure_note).and_return(false)
                expect(::Gitlab).to receive(:create_issue_note).with(project, issue.iid, expected_posted_note)

                expect { subject.invoke! }.to output.to_stdout
              end

              context 'when there is an existing failure comment' do
                let(:note_class) { Struct.new(:id, :body) }
                let(:existing_note) do
                  note_class.new(1, 'Failed most recently in staging pipeline: http://job-failure-url')
                end

                it 'updates the existing comment with the failed job pipeline and url' do
                  expect(subject).to receive(:update_labels).and_call_original
                  expect(::Gitlab).to receive(:edit_issue).with(anything, anything, { labels: ['found:staging.gitlab.com'] })
                  expect(subject).to receive(:post_or_update_failed_job_note).and_call_original
                  expect(subject).to receive(:existing_failure_note).and_return(existing_note)
                  expect(::Gitlab).to receive(:edit_issue_note).with(project, issue.iid, existing_note.id, expected_posted_note)

                  expect { subject.invoke! }.to output.to_stdout
                end
              end
            end

            context 'when test path matches issue title' do
              let(:issue_title) { "Hello #{test_file_partial} world!" }

              it 'finds the issue' do
                expect { subject.invoke! }.to output(/Found issue #{issue.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
              end
            end

            context 'when test matches issue but stacktrace is too different' do
              let(:issues) do
                [issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', test_name, issue_description.gsub('click_submit', 'click_an_entirely_different_button_altogether'))]
              end

              it 'creates the issue in the provided project' do
                expect(::Gitlab).to receive(:upload_file).and_return(file_upload)
                expect(::Gitlab).to receive(:create_issue)
                  .with(
                    project,
                    "Failure in #{test_file_partial} | #{test_name}",
                    hash_including(description: expected_new_issue_description, labels: described_class::NEW_ISSUE_LABELS.to_a + ['found:staging.gitlab.com']))
                  .and_return(new_issue)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          context 'when multiple issues are found' do
            let(:issue1) do
              issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', test_name, issue_description)
            end

            let(:issue2) do
              issue_class.new(2, 'http://existing-issue.url/issue2', 'opened', test_name, issue_description)
            end

            let(:issues) { [issue1, issue2] }

            before do
              allow(subject).to receive(:update_labels)
              allow(subject).to receive(:post_or_update_failed_job_note)
            end

            it 'displays a warning when multiple issues are found and none is a better match than the other' do
              expect { subject.invoke! }.to output(/Too many issues found for test '#{test_name}' \(`#{test_file_full}`\)!/).to_stderr
              expect { subject.invoke! }.not_to output(/Found issue/).to_stdout
              expect { subject.invoke! }.not_to output(/Created new issue/).to_stdout
            end

            context 'when one issue is a better match than the other' do
              let(:issue2) do
                issue_class.new(2, 'http://existing-issue.url/issue2', 'opened', test_name, issue_description.gsub('click_submit', 'click_cancel'))
              end

              it 'returns the best matching issue' do
                expect { subject.invoke! }.to output(/Found issue #{issue1.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
              end

              it 'posts the failed job url' do
                expect(subject).to receive(:update_labels).and_call_original
                expect(::Gitlab).to receive(:edit_issue).with(anything, anything, { labels: ['found:staging.gitlab.com'] })
                allow(subject).to receive(:post_or_update_failed_job_note).and_call_original
                expect(subject).to receive(:existing_failure_note).and_return(false)
                expect(::Gitlab).to receive(:create_issue_note).with(project, issue1.iid, expected_posted_note)

                expect { subject.invoke! }.to output.to_stdout
              end
            end

            context 'when the report has an empty stacktrace' do
              let(:issue2) do
                issue_class.new(2, 'http://existing-issue.url/issue2', 'opened', test_name, issue_description.gsub(/```.+```/m, "```\nCapybara::ElementNotFound: #{failure_message}\n```"))
              end

              let(:test_data) do
                <<~JSON
                  {
                    "examples": [
                      {
                        "full_description": "#{test_name}",
                        "file_path": "#{test_file_full}",
                        "exceptions":[
                          {
                            "class": "Capybara::ElementNotFound",
                            "message": "#{failure_message}",
                            "message_lines": [          ],
                            "backtrace": [          ]
                          }
                        ]
                      }
                    ]
                  }
                JSON
              end

              it 'finds the issue' do
                expect { subject.invoke! }.to output(/Found issue #{issue2.web_url} for test '#{test_name}' with a diff ratio of 0.0%./).to_stdout
              end
            end
          end

          context 'when the test is quarantined' do
            let(:test_data) do
              <<~JSON
                {
                  "examples": [
                    {
                      "id":"#{test_file_full}[1:1:1]",
                      "description":"GET /users",
                      "full_description": "#{test_name}",
                      "status":"failed",
                      "file_path":"#{test_file_full}",
                      "pending_message":null,
                      "testcase":"#{testcase_url}",
                      "quarantine":{
                        "type": "bug",
                        "issue": "https://gitlab.com/quarantine-issue/123"
                      },
                      "screenshot":{},
                      "product_group":null,
                      "ci_job_url":"#{ci_job_url}",
                      "exceptions":[
                        {
                          "class":"RSpec::Expectations::ExpectationNotMetError",
                          "message":"\\nexpected: 404\\n     got: 200\\n\\n(compared using ==)\\n",
                          "message_lines":#{failure_lines.to_json},
                          "backtrace":[
                            "/usr/local/bundle/gems/airborne-0.3.4/lib/airborne/request_expectations.rb:36:in `expect_status'",
                            "./qa/specs/features/api/1_manage/users_spec.rb:14:in `block (3 levels) in <module:QA>'"
                          ]
                        }
                      ]
                    }
                  ]
                }
              JSON
            end

            it 'does not create a new issue' do
              expect(::Gitlab).not_to receive(:create_issue)
              expect(subject).not_to receive(:update_labels)
              expect(subject).not_to receive(:post_or_update_failed_job_note)

              expect { subject.invoke! }.to output.to_stdout
            end

            context 'when there is an existing failure comment' do
              let(:issue_title) { "Hello #{test_name} world!" }
              let(:note_class) { Struct.new(:id, :body) }
              let(:existing_note) do
                note_class.new(1, 'Failed most recently in staging pipeline: http://job-failure-url')
              end

              let(:issue) do
                issue_class.new(1, 'http://existing-issue.url/issue1', 'opened', issue_title, issue_description)
              end

              let(:issues) { [issue] }

              it 'updates the existing comment with the failed job pipeline and url' do
                expect(subject).to receive(:update_labels).and_call_original
                expect(::Gitlab).to receive(:edit_issue).with(project, issue.iid, { labels: ["quarantine", "quarantine::bug", 'found:staging.gitlab.com'] })
                expect(subject).to receive(:post_or_update_failed_job_note).and_call_original
                expect(subject).to receive(:existing_failure_note).and_return(existing_note)
                expect(::Gitlab).to receive(:edit_issue_note).with(project, issue.iid, existing_note.id, expected_posted_note)

                expect { subject.invoke! }.to output.to_stdout
              end
            end
          end

          context 'when the error can be ignored' do
            let(:failure_class) { 'Net::ReadTimeout' }
            let(:failure_reason) { "error was #{failure_class}" }

            it_behaves_like 'error(s) can be ignored'
          end

          context 'with multiple errors' do
            let(:failure_class) { 'Error' }
            let(:failure_class2) { 'Error' }
            let(:test_data) do
              <<~JSON
                {
                  "examples": [
                    {
                      "full_description": "#{test_name}",
                      "file_path": "#{test_file_full}",
                      "screenshot": { "image": "failure_screenshot.png"},
                      "exceptions": [
                        {
                          "class": "#{failure_class}",
                          "message": "An Error Here",
                          "message_lines": #{failure_lines.to_json},
                          "backtrace":[
                            "/usr/local/bundle/gems/airborne-0.3.4/lib/airborne/request_expectations.rb:36:in `expect_status'",
                            "./qa/specs/features/api/1_manage/users_spec.rb:14:in `block (3 levels) in <module:QA>'"
                          ]
                        },
                        {
                          "class": "#{failure_class2}",
                          "message": "An Error Here",
                          "message_lines": #{failure_lines.to_json},
                          "backtrace":[
                            "/usr/local/bundle/gems/airborne-0.3.4/lib/airborne/request_expectations.rb:36:in `expect_status'",
                            "./qa/specs/features/api/1_manage/users_spec.rb:14:in `block (3 levels) in <module:QA>'"
                          ]
                        }
                      ]
                    }
                  ]
                }
              JSON
            end

            context 'when all errors can be ignored' do
              let(:failure_class) { 'Net::ReadTimeout' }
              let(:failure_class2) { 'Net::ReadTimeout' }
              let(:failure_reason) { "errors were #{failure_class}, #{failure_class2}" }

              it_behaves_like 'error(s) can be ignored'
            end

            context 'when only one error can be ignored' do
              let(:failure_class) { 'Net::ReadTimeout' }

              it_behaves_like 'new issue created'
            end

            context 'when no error can be ignored' do
              it_behaves_like 'new issue created'
            end
          end
        end
      end
    end
  end
end
