# frozen_string_literal: true

describe Gitlab::QA::Report::GitlabIssueClient do
  context 'with valid input' do
    let(:gitlab_client_config) { double('GitLab client config') }

    before do
      allow(::Gitlab).to receive(:configure).and_yield(gitlab_client_config)
      allow(gitlab_client_config).to receive(:endpoint=)
      allow(gitlab_client_config).to receive(:private_token=)

      described_class.new(token: 'token', project: 'project')
    end

    context 'when the GitLab client is configured' do
      it 'passes the token to the GitLab client' do
        expect(gitlab_client_config).to have_received(:private_token=).with('token')
      end

      it 'uses the default base API URL' do
        expect(gitlab_client_config).to have_received(:endpoint=).with('https://gitlab.com/api/v4')
      end
    end

    context 'when the base API URL is specified as an environment variable' do
      around do |example|
        ClimateControl.modify(GITLAB_API_BASE: 'http://another.gitlab.url') { example.run }
      end

      it 'uses the specified URL' do
        expect(gitlab_client_config).to have_received(:endpoint=).with('http://another.gitlab.url')
      end
    end

    context 'when using method' do
      let(:client) { spy('gitlab client') }

      subject { described_class.new(token: 'token', project: 'project') }

      before do
        stub_const('Gitlab', client)
      end

      it '#edit_issue_note calls GitLab' do
        subject.edit_issue_note(issue_iid: 1, note_id: 2, note: 'note')

        expect(client).to have_received(:edit_issue_note).with('project', 1, 2, 'note')
      end

      it '#find_issue_notes calls GitLab' do
        subject.find_issue_notes(iid: 1)

        expect(client).to have_received(:issue_notes).with('project', 1, order_by: 'created_at', sort: 'asc')
      end
    end
  end
end
